/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    // Add any custom configuration if needed
  };
  
  export default nextConfig;
  