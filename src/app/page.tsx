// pages/index.js
import Link from "next/link";
import { BackgroundGradientAnimation } from "@/components/background-gradient-animation";
import { TextGenerateEffect } from "@/components/text-generate-effect";

const words = `Entropy-com`;

const BackgroundGradientAnimationDemo = () => {
  return (
    <div>
      <title>Home</title> {/* Set your title here */}
      <link rel="icon" href="/images/logo.jpg" type="image/jpg" />
      <BackgroundGradientAnimation>
        <div className="text-white text-center md:text-4xl lg:text-7xl">
          <Link href="/about">
            <span className="space-between nav1">About</span>
          </Link>
          <Link href="/vision">
            <span className="space-between nav2">Vision</span>
          </Link>
          <Link href="/ideas">
            <span className="space-between nav3">Ideas</span>
          </Link>
        </div>
        <div className="absolute z-50 inset-0 flex items-center justify-center text-white font-bold px-4 pointer-events-none text-3xl text-center md:text-4xl lg:text-7xl">
          <TextGenerateEffect words={words} className="generated-text" />
          <img
            src="/images/image.png"
            alt="Your alt text"
            className="image-moon"
          />
        </div>
      </BackgroundGradientAnimation>
    </div>
  );
};

export default BackgroundGradientAnimationDemo;
